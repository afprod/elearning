<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class untuk resource login
 *
 * @package   e-Learning Dokumenary Net
 * @author    Almazari <almazary@gmail.com>
 * @copyright Copyright (c) 2013 - 2016, Dokumenary Net.
 * @since     1.0
 * @link      http://dokumenary.net
 *
 * INDEMNITY
 * You agree to indemnify and hold harmless the authors of the Software and
 * any contributors for any direct, indirect, incidental, or consequential
 * third-party claims, actions or suits, as well as any related expenses,
 * liabilities, damages, settlements or fees arising from your use or misuse
 * of the Software, or a violation of any terms of this license.
 *
 * DISCLAIMER OF WARRANTY
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR
 * IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF QUALITY, PERFORMANCE,
 * NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * LIMITATIONS OF LIABILITY
 * YOU ASSUME ALL RISK ASSOCIATED WITH THE INSTALLATION AND USE OF THE SOFTWARE.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS OF THE SOFTWARE BE LIABLE
 * FOR CLAIMS, DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF, OR IN CONNECTION
 * WITH THE SOFTWARE. LICENSE HOLDERS ARE SOLELY RESPONSIBLE FOR DETERMINING THE
 * APPROPRIATENESS OF USE AND ASSUME ALL RISKS ASSOCIATED WITH ITS USE, INCLUDING
 * BUT NOT LIMITED TO THE RISKS OF PROGRAM ERRORS, DAMAGE TO EQUIPMENT, LOSS OF
 * DATA OR SOFTWARE PROGRAMS, OR UNAVAILABILITY OR INTERRUPTION OF OPERATIONS.
 */

class Script extends MY_Controller
{
    function index()
    {
    	$this->db2->select("sekolah.nama_sekolah");
    	$this->db2->from("sekolah");
    	$this->db2->join("kuota_peserta","sekolah.id_sekolah = kuota_peserta.id_sekolah");
    	$get_sekolah = $this->db2->get();
        // $get_sekolah = $this->db2->query("SELECT DISTINCT `sekolah`.nama_sekolah FROM `sekolah`, `kuota_peserta` WHERE `sekolah`.id_sekolah = `kuota_peserta`.id_sekolah");
        foreach ($get_sekolah->result() as $one_sekolah) {
        	// var_dump($one_sekolah->nama_sekolah);die();
        	$username = strtolower($one_sekolah->nama_sekolah);
        	$username = str_replace("negeri", "n", $username);
        	$username = str_replace(" ", "", $username);
        	$password = "00000";
        	$nis           = "0000";
            $nama          = "Trainee ".$one_sekolah->nama_sekolah;
            $jenis_kelamin = "";
            $tahun_masuk   = null;
            $tempat_lahir  = "";
            $tgl_lahir     = "";
            $agama         = null;
            $alamat        = null;
            $kelas_id = 19;
            $foto = null;
            $status_id=1;

            // var_dump($get_keterampilan['nama'],$kelas1['nama']);die();

            # simpan data siswa
            $siswa_id = $this->siswa_model->create(
                $nis,
                $nama,
                $jenis_kelamin,
                $tempat_lahir,
                null,
                $agama,
                $alamat,
                $tahun_masuk,
                $foto,
                "Tata Boga",
                "Makanan Pastry",
                $status_id
            );

            # simpan data login
            $this->login_model->create(
                $username,
                $password,
                $siswa_id,
                null
            );

            // simpan kelas siswa
            $this->kelas_model->create_siswa(
                $kelas_id,
                $siswa_id,
                1
            );

        }
        die();
    }

    function trainer()
    {
        $this->db2->select("sekolah.nama_sekolah");
        $this->db2->from("sekolah");
        $this->db2->join("kuota_peserta","sekolah.id_sekolah = kuota_peserta.id_sekolah");
        $get_sekolah = $this->db2->get();
        // $get_sekolah = $this->db2->query("SELECT DISTINCT `sekolah`.nama_sekolah FROM `sekolah`, `kuota_peserta` WHERE `sekolah`.id_sekolah = `kuota_peserta`.id_sekolah");
        foreach ($get_sekolah->result() as $one_sekolah) {
            $username = strtolower($one_sekolah->nama_sekolah);
            $username = str_replace("negeri", "n", $username);
            $username = str_replace(" ", "", $username);
            $password = "00000";
            $nip           = "0000";
            for ($i=1;$i<9;$i++){
                $username_baru = "t".$i."-".$username;
                $nama          = "Trainer-".$i." ".$one_sekolah->nama_sekolah;
                $jenis_kelamin = "";
                $tahun_masuk   = null;
                $tempat_lahir  = "";
                $tgl_lahir     = "";
                $agama         = null;
                $alamat        = null;
                $foto = null;
                $status_id=1;

                # simpan data trainer

                $trainer_id = $this->pengajar_model->create(
                    $nip,
                    $nama,
                    $jenis_kelamin,
                    $tempat_lahir,
                    null,
                    $alamat,
                    $foto,
                    $status_id
                );

                $this->login_model->create(
                    $username_baru,
                    $password,
                    null,
                    $trainer_id,
                    0
                );
            }

        }
        die();
    }
}

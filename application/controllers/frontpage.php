<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class untuk halaman utama
 *
 * @package   e-Learning Dokumenary Net
 * @author    Almazari <almazary@gmail.com>
 * @copyright Copyright (c) 2013 - 2016, Dokumenary Net.
 * @since     1.0
 * @link      http://dokumenary.net
 *
 * INDEMNITY
 * You agree to indemnify and hold harmless the authors of the Software and
 * any contributors for any direct, indirect, incidental, or consequential
 * third-party claims, actions or suits, as well as any related expenses,
 * liabilities, damages, settlements or fees arising from your use or misuse
 * of the Software, or a violation of any terms of this license.
 *
 * DISCLAIMER OF WARRANTY
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR
 * IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF QUALITY, PERFORMANCE,
 * NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * LIMITATIONS OF LIABILITY
 * YOU ASSUME ALL RISK ASSOCIATED WITH THE INSTALLATION AND USE OF THE SOFTWARE.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS OF THE SOFTWARE BE LIABLE
 * FOR CLAIMS, DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF, OR IN CONNECTION
 * WITH THE SOFTWARE. LICENSE HOLDERS ARE SOLELY RESPONSIBLE FOR DETERMINING THE
 * APPROPRIATENESS OF USE AND ASSUME ALL RISKS ASSOCIATED WITH ITS USE, INCLUDING
 * BUT NOT LIMITED TO THE RISKS OF PROGRAM ERRORS, DAMAGE TO EQUIPMENT, LOSS OF
 * DATA OR SOFTWARE PROGRAMS, OR UNAVAILABILITY OR INTERRUPTION OF OPERATIONS.
 */

class Frontpage extends MY_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'pager', 'parser', 'image_lib', 'upload', 'twig', 'user_agent', 'email', 'menu','slice'));
    }
    function index(){
        $this->db->like('konten','<iframe');
        $this->db->order_by('views','DESC');
        $this->db->limit(8);
        $get_materi = $this->db->get('materi');
        $get_materi = $get_materi->result();
        
        $this->db->like('konten','<iframe');
        $this->db->order_by('tgl_posting','DESC');
        $this->db->limit(8);

        $get_materi_terbaru = $this->db->get('materi');
        $get_materi_terbaru = $get_materi_terbaru->result();

        $get_topik = $this->db->get('mapel');
        $get_topik = $get_topik->result();
        foreach ($get_topik as $key) {
            $this->db->where('mapel_id',$key->id);
            $key->jumlah_materi = $this->db->get('materi')->num_rows();
        }
        $this->slice->view('frontpage/index',['get_materi' => $get_materi , 'topik' => $get_topik, 'get_materi_terbaru' => $get_materi_terbaru]);
    }
    public function tentang()
	{	
		$this->slice->view('frontpage/tentang');
	}

	public function kontak()
	{	
		$this->slice->view('frontpage/kontak');
	}

	public function ruangBaca()
	{	
		$this->slice->view('frontpage/ruang_baca');
	}

    public function cari()
    {   
        $get_topik = $this->db->get('mapel');
        $get_topik = $get_topik->result();
        foreach ($get_topik as $key) {
            $this->db->where('mapel_id',$key->id);
            $key->jumlah_materi = $this->db->get('materi')->num_rows();
        }
        $this->slice->view('frontpage/cari',['topik'=>$get_topik]);
    }

    public function cariAction()
    {
        if(empty($this->input->post('keyword'))){
            $this->db->like('konten','<iframe');
            $this->db->order_by('views','DESC');
            $this->db->limit(8);
            $get_materi = $this->db->get('materi');
            $get_materi = $get_materi->result();
            $output='';
            foreach($get_materi as $materi)
            {
                $judul_view = $materi->judul;
                if(strpos($judul_view,'Bab ') !== false){
                    $loc = strpos($judul_view,'Bab ')+4;
                    while($judul_view[$loc]!== ' '){
                        $loc+=1;
                    }
                    $judul_view = substr($judul_view,$loc+1);
                }
                $thumb_idx = strpos($materi->konten,'youtube.com/embed/')+18;
                $len = 0;
                while($materi->konten[$thumb_idx+$len] != '"'){
                    $len+=1;
                }
                $thumbnail = substr($materi->konten,$thumb_idx,$len);
                $output .= '<div class="col-md-3">
                        <div class="card" style="width: 18rem;height: 100%">
                          <img src="https://img.youtube.com/vi/'.$thumbnail.'/hqdefault.jpg" class="card-img-top" alt="...">
                          <div class="card-body flex-column">
                            <h6 class="card-title">'.$judul_view.'</h6>
                            <br><br>
                            <p class="card-text mt-auto" style="position: absolute;bottom: 0px;right: 20px;"><i class="fa fa-eye" aria-hidden="true"></i> '.$materi->views.'</p>
                            <a href="'.site_url('materi/detail/'.$materi->id).'" class="button button-black" style="position: absolute;bottom: 10px;">Detail</a>
                          </div>
                        </div>
                    </div>';
            }
            echo $output;
        }
        else{
            $keyword = $_POST['keyword'];
            $this->db->like('judul',$keyword);
            $this->db->like('konten','<iframe');
            $get_materi = $this->db->get('materi');
            $get_materi = $get_materi->result();
            $output='';
            foreach($get_materi as $materi)
            {
                $judul_view = $materi->judul;
                if(strpos($judul_view,'Bab ') !== false){
                    $loc = strpos($judul_view,'Bab ')+4;
                    while($judul_view[$loc]!== ' '){
                        $loc+=1;
                    }
                    $judul_view = substr($judul_view,$loc+1);
                }
                $thumb_idx = strpos($materi->konten,'youtube.com/embed/')+18;
                $len = 0;
                while($materi->konten[$thumb_idx+$len] != '"'){
                    $len+=1;
                }
                $thumbnail = substr($materi->konten,$thumb_idx,$len);
                $output .= '<div class="col-md-3">
                        <div class="card" style="width: 18rem;height: 100%">
                          <img src="https://img.youtube.com/vi/'.$thumbnail.'/hqdefault.jpg" class="card-img-top" alt="...">
                          <div class="card-body flex-column">
                            <h6 class="card-title">'.$judul_view.'</h6>
                            <br><br>
                            <p class="card-text mt-auto" style="position: absolute;bottom: 0px;right: 20px;"><i class="fa fa-eye" aria-hidden="true"></i> '.$materi->views.'</p>
                            <a href="'.site_url('materi/detail/'.$materi->id).'" class="button button-black" style="position: absolute;bottom: 10px;">Detail</a>
                          </div>
                        </div>
                    </div>';
            }
            echo $output;
        }
    }

    public function kegiatan()
    {
        $get_topik = $this->mapel_model->retrieve_all_mapel();
        $results = array();
        foreach ($get_topik as $topik)
        {
            $jurnals = $this->jurnal_model->retrieve_all(
                8,
                1,
                array(),
                null,
                null,
                null,
                null,
                false,
                $topik['id']);
          
            if(!empty($jurnals)){
                $results[$topik['id']]['topik'] = $topik;
                $results[$topik['id']]['jurnal'] = $jurnals;
            }
        }
        
        $get_topik = $this->db->get('mapel');
        $get_topik = $get_topik->result();
        foreach ($get_topik as $key) {
            $this->db->where('mapel_id',$key->id);
            $key->jumlah_materi = $this->db->get('jurnal')->num_rows();
        }
       
        $this->slice->view('frontpage/list_kegiatan',['result'=>$results,'topik'=>$get_topik]);
    }

    public function kegiatan_siswa($segment_3='')
    {
        if(is_login() && is_siswa()){
            $siswa_session = get_sess_data('user','id');
            $page_no = (int)$segment_3;
            if (empty($page_no)) {
                $page_no = 1;
            }
            $siswa_id = $siswa_session;
            $retrieve_all_jurnal = $this->jurnal_model->retrieve_all(
                20,
                $page_no,
                $siswa_id,
                null,
                null,
                null,
                null,
                true
            );
            $results = array();
            foreach ($retrieve_all_jurnal['results'] as $key => $val) {
                $results[$key] = $val;
                $this->db->select('nama');
                $this->db->from('mapel');
                $this->db->where('id',$results[$key]['mapel_id']);
                $result = $this->db->get();
                $mapel = $result->row_array();
                $results[$key]['mapel']= $mapel['nama'];
            }

            $jurnal     = $results;
            $pagination  = $this->pager->view($retrieve_all_jurnal, 'jurnal/index/');
            $this->slice->view('frontpage/kegiatan_siswa',['jurnal'=>$jurnal,'pagination'=>$pagination]);
        }else{
            redirect('login/index');
        }
    }

    public function detail_kegiatan($segment_3='')
    {
        $kegiatan_id = (int)$segment_3;
        $jurnal = $this->jurnal_model->retrieve($kegiatan_id);
        $get_nama = $this->siswa_model->retrieve($jurnal['siswa_id']);
        $jurnal['nama_penulis'] = $get_nama['nama'];
        $get_mapel = $this->mapel_model->retrieve($jurnal['mapel_id']);
        $jurnal['mapel'] = $get_mapel['nama'];

        $siswa_id = $jurnal['siswa_id'];
        $retrieve_all_jurnal = $this->jurnal_model->retrieve_all(
            4,
            1,
            $siswa_id,
            null,
            null,
            null,
            null,
            true
        );
        $results = array();
        foreach ($retrieve_all_jurnal['results'] as $key => $val) {
            $results[$key] = $val;
            $this->db->select('nama');
            $this->db->from('mapel');
            $this->db->where('id',$results[$key]['mapel_id']);
            $result = $this->db->get();
            $mapel = $result->row_array();
            $results[$key]['mapel']= $mapel['nama'];
            $get_nama = $this->siswa_model->retrieve($results[$key]['siswa_id']);
            $results[$key]['nama_penulis'] = $get_nama['nama'];
        }

        $this->slice->view('frontpage/detail_kegiatan',['jurnal'=>$jurnal,'lainlain'=>$results]);
    }

    public function sekolah()
    {   
        $this->slice->view('frontpage/sekolah');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

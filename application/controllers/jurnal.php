<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurnal extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('jurnal_model');
    }

    function index($segment_3='')
    {
        if(is_login() && is_siswa()){
            $siswa_session = get_sess_data('user','id');
            $page_no = (int)$segment_3;
            if (empty($page_no)) {
                $page_no = 1;
            }
            $siswa_id = $siswa_session;
            if(!empty($_GET['mapel']))
            {
                $mapel_id = $_GET['mapel'];
            }
            $retrieve_all_jurnal = $this->jurnal_model->retrieve_all(
                20,
                $page_no,
                $siswa_id,
                null,
                null,
                null,
                null,
                true
            );
            $results = array();
            foreach ($retrieve_all_jurnal['results'] as $key => $val) {
                $results[$key] = $val;
            }

            $data['jurnal']      = $results;
            $data['pagination']  = $this->pager->view($retrieve_all_jurnal, 'jurnal/index/');
            $this->twig->display('list-jurnal.html', $data);
        }
        else{
            if(!empty($_GET['mapel']))
            {
                $mapel_id = $_GET['mapel'];
            }

            $retrieve_all_jurnal = $this->jurnal_model->retrieve_all(
                20,
                1,
                null,
                null,
                null,
                null,
                null,
                true,
                $mapel_id
            );
            $results = array();
            foreach ($retrieve_all_jurnal['results'] as $key => $val) {
                $results[$key] = $val;
            }

            $data['jurnal']      = $results;
            $data['pagination']  = $this->pager->view($retrieve_all_jurnal, 'jurnal/index/');
            $this->twig->display('list-jurnal.html', $data);
        }

        // if(is_login() && is_pengajar()){
        //     $pengajar_session = get_sess_data('user','id');
        // }

    }

    function tambah()
    {
        must_login();
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                return $this->twig->display('tambah-kegiatan-siswa.html');
                break;
            case 'POST':
                if(is_login() && is_siswa()){
                    $siswa_session = get_sess_data('user','id');
                }else{
                    redirect(site_url('login'));
                }
                $success = false;

                $this->db->select('mapel_id');
                $this->db->from('mapel_kelas');
                $this->db->join('kelas_siswa', 'mapel_kelas.kelas_id = kelas_siswa.kelas_id');
                $this->db->where('kelas_siswa.siswa_id',$siswa_session);
                $get_mapel = $this->db->get();
                $mapel_id = $get_mapel->row_array();
                $mapel_id = $mapel_id['mapel_id'];
                $siswa_id = $siswa_session;
                $judul    = $this->input->post('judul', TRUE);
                $tanggal  = $this->input->post('tanggal', TRUE);
                $pukul    = $this->input->post('pukul', TRUE);
                $kegiatan    = $this->input->post('kegiatan', TRUE);
                $file = null;
                $instagram = null;
                //
                if($this->input->post('media')){
                    $config['upload_path']   = get_path_file();
                    $config['allowed_types'] = 'jpg|jpeg|JPG|JPEG|png|mp4|mpg|mpeg';
                    $config['max_size']      = '0';
                    $config['max_width']     = '0';
                    $config['max_height']    = '0';
                    $config['file_name']     = url_title($this->input->post('judul', TRUE).'_'.time(), '_', TRUE);
                    
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('media')){
                        $upload_data = $this->upload->data();
                        $file        = $upload_data['file_name'];
                    }else{
                        $upload_data = $this->upload->data();
                        if (!empty($upload_data) AND is_file(get_path_file($upload_data['file_name']))) {
                            unlink(get_path_file($upload_data['file_name']));
                        }
                        $data['error_upload'] = '<span class="text-error">'.$this->upload->display_errors().'</span>';
                        $this->session->set_flashdata('jurnal', get_alert('error', $this->upload->display_errors()));
                        redirect(site_url('jurnal/tambah'));
                    }
                }
                if($this->input->post('instagram'))
                {
                    $instagram_post = $this->input->post('instagram');
                    $loc = strpos($instagram_post,'/p/')+3;
                    while($instagram_post[$loc]!== '/'){
                        $loc+=1;
                    }
                    $instagram = substr($instagram_post,0,$loc+1);
                }
                // 
                $data = array(
                    'judul'       => $judul,
                    'tanggal'     => $tanggal,
                    'pukul'       => $pukul,
                    'deskripsi'    => $kegiatan,
                    'media'       => $file,
                    'instagram'   => $instagram,
                    'siswa_id'    => $siswa_id,
                    'mapel_id'    => $mapel_id,
                    'tgl_posting' => date('Y-m-d H:i:s')
                );
                $this->db->insert('jurnal', $data);
                $this->db->insert_id();
                //
                $this->session->set_flashdata('jurnal', get_alert('success', 'Jurnal berhasil disimpan'));
                return redirect(site_url('jurnal'));

                $success = true;
                break;
        }

    }

    function detail($segment_3 = '', $segment_4 = '', $segment_5 = '')
    {
        // must_login();
        must_login();
        $jurnal_id = (int)$segment_3;

        if (empty($jurnal_id)) {
            show_404();
        }

        $jurnal = $this->jurnal_model->retrieve($jurnal_id);
        if (empty($jurnal)) {
            show_404();
        }

        if (!is_siswa()) {
            show_404();
        }
        
        $data['jurnal'] = $jurnal;
        $this->twig->display('detail-jurnal.html', $data);
    }

     function delete($segment_3 = '', $segment_4 = '')
    {
        must_login();

        $materi_id = (int)$segment_3;
        $uri_back  = (string)$segment_4;

        if (empty($uri_back)) {
            $uri_back = site_url('materi');
        } else {
            $uri_back = deurl_redirect($uri_back);
        }

        $materi = $this->jurnal_model->retrieve($materi_id);
        if (empty($materi)) {
            redirect($uri_back);
        }

        if (is_siswa() AND $materi['siswa_id'] != get_sess_data('user', 'id')) {
            redirect($uri_back);
        }

        # jika file
        if (!empty($materi['media']) AND is_file(get_path_file($materi['media']))) {
            unlink(get_path_file($materi['media']));
        }

        $this->jurnal_model->delete($materi['id']);

        $this->session->set_flashdata('jurnal', get_alert('warning', 'Jurnal berhasil dihapus.'));
        redirect($uri_back);
    }
        
}

<?php

/**
 * Class Model untuk resource Materi
 *
 * @package Elearning Dokumenary
 * @link    http://www.dokumenary.net
 */
class Jurnal_model extends CI_Model
{
    /**
     * Method untuk menghitung data tertentu
     * @param  string $by
     */

    public function retrieve_all(
        $no_of_records = 10,
        $page_no       = 1,
        $siswa_id      = array(),
        $judul         = null,
        $konten        = null,
        $tgl_posting   = null,
        $pukul         = null,
        $pagination    = true,
        $mapel_id      = null
    ) {
        $no_of_records = (int)$no_of_records;
        $page_no       = (int)$page_no;

        $where    = array();
        $group_by = array();
        if (!empty($siswa_id)) {
            $operation = 'where_in';
            $where['jurnal.siswa_id'] = array($siswa_id, $operation);
        }
        $like = 0;
        if (!empty($judul)) {
            $where['jurnal.judul'] = array($judul, 'like');
            $like = 1;
        }
        if (!empty($konten)) {
            if ($like) {
                $value = array($like, 'or_like');
            } else {
                $value = array($like, 'like');
            }
            $where['jurnal.konten'] = array($like, 'like');
        }
        if (!empty($tgl_posting)) {
            $where['DATE(tgl_posting)'] = array($tgl_posting, 'where');
        }

        if (!empty($mapel_id)) {
            $where['jurnal.mapel_id'] = array($mapel_id, 'where');
        }

        $orderby = array(
            'jurnal.id' => 'DESC'
        );

        if ($pagination) {
            $data = $this->pager->set('jurnal', $no_of_records, $page_no, $where, $orderby, 'jurnal.*', $group_by);
        } else {
            # cari jumlah semua pengajar
            $no_of_records = $this->db->count_all('jurnal');
            $search_all    = $this->pager->set('jurnal', $no_of_records, $page_no, $where, $orderby, 'jurnal.*', $group_by);
            $data          = $search_all['results'];
        }

        return $data;
    }

    public function retrieve($id)
    {
        $id = (int)$id;

        $this->db->where('id', $id);
        $result = $this->db->get('jurnal', 1);
        return $result->row_array();
    }

    public function create(
        $siswa_id    = null,
        $judul = null,
        $kegiatan  = null,
        $tanggal  = null,
        $pukul  = null,
        $file    = null,
        $publish = 1
    ) {
        $data = array(
            'siswa_id'    => $siswa_id,
            'judul'       => $judul,
            'deskripsi'   => $kegiatan,
            'media'        => $file,
            'tanggal'     => $tanggal,
            'pukul'       => $pukul,
            'tgl_posting' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('jurnal', $data);
        return $this->db->insert_id();
    }

    public function delete($id)
    {
        $id = (int)$id;

        $this->db->where('id', $id);
        $this->db->delete('jurnal');
        return true;
    }

}

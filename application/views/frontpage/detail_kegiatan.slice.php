@extends('layouts.front.app')
@section('judul', ' Kegiatan Saya')


@section('header')
@include('layouts.front.header')
@endsection

@section('content')
<div class="breadcrumb-banner-area pt-150 pb-85 bg-3" style="background:url('<?php echo base_url('assets/img/BackGround.png'); ?>') no-repeat scroll center top / cover;">
	<div style="background-color: rgba(85, 85, 85, 0.3);position: absolute;top: 0;left: 0;width: 100%;height: 100%;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-text">
                    <h2 class="text-center">Detail Kegiatan</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog-area ptb-130 ptb-sm-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="single-blog hover-effect mb-50">
                    <div class="blog-image">
                        <img src="{{$jurnal['instagram'].'media/?size=l'}}" alt="">
                    </div>
                    <div class="blog-text">
                        <h4><a href="blog-details.html">{{$jurnal['judul']}}</a></h4>
                        <div class="blog-post-info">
                            <span>{{$jurnal['tanggal']}}</span>
                            <span>{{$jurnal['nama_penulis']}}</span>
                        </div>
                        <p class="mb-25">{{$jurnal['deskripsi']}}</p>
                        <p>Link Instagram : <a href="{{$jurnal['instagram']}}">{{$jurnal['judul']}}</a></p>
                    </div>
                </div>
                <div class="tags-and-links fix pt-14 pb-12">
                    <div class="related-tag pull-left">
                        <span class="mr-10">Mata Pelajaran</span>
                        <ul class="tags">
                            <li><a href="#">{{$jurnal['mapel']}}</a></li>
                        </ul>
                    </div>
                    <!-- <div class="social-links pull-right">
                        <span>Share:</span>
                        <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                        <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                        <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                        <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                    </div> -->
                </div>
                <!-- <div class="comments fix pt-55">
                    <h4 class="title pb-8 mb-27">Comments</h4>
                    <div class="single-comment fix mb-30">
                        <div class="author-image">
                            <img alt="" src="images/comment/1.jpg">
                        </div>
                        <div class="comment-text fix">
                            <div class="author-info">
                                <h4 class="mb-8"><a href="#">MD Abul Miya</a></h4>
                                <span class="reply"><a href="#">Reply</a></span>
                                <span class="comment-time">Posted on Mar 11, 2018 /</span>
                            </div>
                            <p class="mb-18">There are many variations of passages of Lorem Ipsum available, but the majority have suffered aation in some form, by injected humour,</p>
                        </div>
                    </div>
                    <div class="single-comment fix mb-30 ml-130">
                        <div class="author-image">
                            <img alt="" src="images/comment/2.jpg">
                        </div>
                        <div class="comment-text fix">
                            <div class="author-info">
                                <h4 class="mb-8"><a href="#">Md Robiul Siddikee</a></h4>
                                <span class="reply"><a href="#">Reply</a></span>
                                <span class="comment-time">Posted on Mar 15, 2018 /</span>
                            </div>
                            <p class="mb-18">There are many variations of passages of Lorem Ipsum available, but the majority have suffered aation in some form, by injected humour,</p>
                        </div>
                    </div>
                    <div class="single-comment fix mb-30">
                        <div class="author-image">
                            <img alt="" src="images/comment/3.jpg">
                        </div>
                        <div class="comment-text fix">
                            <div class="author-info">
                                <h4 class="mb-8"><a href="#"> Samia Sultana</a></h4>
                                <span class="reply"><a href="#">Reply</a></span>
                                <span class="comment-time">Posted on Feb 12, 2018 /</span>
                            </div>
                            <p class="mb-18">There are many variations of passages of Lorem Ipsum available, but the majority have suffered aation in some form, by injected humour,</p>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="col-lg-3 sidebar-right">
                <div class="single-sidebar-widget mb-48">
                    <div class="sidebar-widget-title">
                        <h4><span>Kegiatan Lainnya</span></h4>
                    </div>
                    <div class="recent-posts">
                    	@foreach($lainlain as $lain)
                    	@if($lain['id']!=$jurnal['id'])
                        <div class="recent-post-item ptb-20">
                            <h5 class="mb-6"><a href="{{site_url('frontpage/detail_kegiatan/'.$lain['id'])}}">{{$lain['judul']}}</a></h5>
                            <span class="block"><i class="zmdi zmdi-calendar-check mr-10"></i>{{$lain['tanggal']}}</span>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br></br>
<br></br>
<br></br>
@endsection

@section('footer')
    @include('layouts.front.footer')
@endsection

@section('moreJS')
<script>

</script>
@endsection

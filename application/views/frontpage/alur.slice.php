<!-- Working Aera Start -->
<script src="https://use.fontawesome.com/548ece7013.js"></script>
<div class="woring-area pt-130 pb-100 pt-sm-60 pb-sm-30">
    <div class="container">
        <div class="job-post-area pb-100 pb-sm-35">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center ">
                        <h2>Materi Terbaru</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @for($i=0;$i<4;$i++)
                <?php
                    $judul_view = $get_materi_terbaru[$i]->judul;
                    if(strpos($judul_view,'Bab ') !== false){
                        $loc = strpos($judul_view,'Bab ')+4;
                        while($judul_view[$loc]!== ' '){
                            $loc+=1;
                        }
                        $judul_view = substr($judul_view,$loc+1);
                    }
                    $thumb_idx = strpos($get_materi_terbaru[$i]->konten,'youtube.com/embed/')+18;
                    $len = 0;
                    while($get_materi_terbaru[$i]->konten[$thumb_idx+$len] != '"'){
                        $len+=1;
                    }
                    $thumbnail = substr($get_materi_terbaru[$i]->konten,$thumb_idx,$len);
                    $thumbnail = explode('?',$thumbnail);
                    $thumbnail = $thumbnail[0];
                ?>
                <div class="col-md-3" style="margin-bottom: 3px;">
                    <div class="card" style="width: 18rem;height: 100%;">
                      <img src="https://img.youtube.com/vi/{{$thumbnail}}/hqdefault.jpg" class="card-img-top" alt="...">
                      <div class="card-body flex-column">
                        <h6 class="card-title">{{$judul_view}}</h6>
                        <br><br>
                        <p class="card-text mt-auto" style="position: absolute;bottom: 0px;right: 20px;"><i class="fa fa-eye" aria-hidden="true"></i> {{$get_materi_terbaru[$i]->views}}</p>
                        <a href="{{site_url('materi/detail/'.$get_materi_terbaru[$i]->id)}}" class="button button-black" style="position: absolute;bottom: 10px;">Detail</a>
                      </div>
                    </div>
                </div>
                @endfor
            </div>
            <br>
            <div class="row">
                @for($i=4;$i<8;$i++)
                <?php
                    $judul_view = $get_materi_terbaru[$i]->judul;
                    if(strpos($judul_view,'Bab ') !== false){
                        $loc = strpos($judul_view,'Bab ')+4;
                        while($judul_view[$loc]!== ' '){
                            $loc+=1;
                        }
                        $judul_view = substr($judul_view,$loc+1);
                    }
                    $thumb_idx = strpos($get_materi_terbaru[$i]->konten,'youtube.com/embed/')+18;
                    $len = 0;
                    while($get_materi_terbaru[$i]->konten[$thumb_idx+$len] != '"'){
                        $len+=1;
                    }
                    $thumbnail = substr($get_materi_terbaru[$i]->konten,$thumb_idx,$len);
                    $thumbnail = explode('?',$thumbnail);
                    $thumbnail = $thumbnail[0];
                ?>
                <div class="col-md-3">
                    <div class="card" style="width: 18rem;height: 100%">
                      <img src="https://img.youtube.com/vi/{{$thumbnail}}/hqdefault.jpg" class="card-img-top" alt="...">
                      <div class="card-body flex-column">
                        <h6 class="card-title">{{$judul_view}}</h6>
                        <br><br>
                        <p class="card-text mt-auto" style="position: absolute;bottom: 0px;right: 20px;"><i class="fa fa-eye" aria-hidden="true"></i> {{$get_materi_terbaru[$i]->views}}</p>
                        <a href="{{site_url('materi/detail/'.$get_materi_terbaru[$i]->id)}}" class="button button-black" style="position: absolute;bottom: 10px;">Detail</a>
                      </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
        <div class="job-post-area pb-100 pb-sm-35">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center ">
                        <h2>Materi Paling Populer</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @for($i=0;$i<4;$i++)
                <?php
                    $judul_view = $get_materi[$i]->judul;
                    if(strpos($judul_view,'Bab ') !== false){
                        $loc = strpos($judul_view,'Bab ')+4;
                        while($judul_view[$loc]!== ' '){
                            $loc+=1;
                        }
                        $judul_view = substr($judul_view,$loc+1);
                    }
                    $thumb_idx = strpos($get_materi[$i]->konten,'youtube.com/embed/')+18;
                    $len = 0;
                    while($get_materi[$i]->konten[$thumb_idx+$len] != '"'){
                        $len+=1;
                    }
                    $thumbnail = substr($get_materi[$i]->konten,$thumb_idx,$len);
                    $thumbnail = explode('?',$thumbnail);
                    $thumbnail = $thumbnail[0];
                ?>
                <div class="col-md-3" style="margin-bottom: 3px;">
                    <div class="card" style="width: 18rem;height: 100%;">
                      <img src="https://img.youtube.com/vi/{{$thumbnail}}/hqdefault.jpg" class="card-img-top" alt="...">
                      <div class="card-body flex-column">
                        <h6 class="card-title">{{$judul_view}}</h6>
                        <br><br>
                        <p class="card-text mt-auto" style="position: absolute;bottom: 0px;right: 20px;"><i class="fa fa-eye" aria-hidden="true"></i> {{$get_materi[$i]->views}}</p>
                        <a href="{{site_url('materi/detail/'.$get_materi[$i]->id)}}" class="button button-black" style="position: absolute;bottom: 10px;">Detail</a>
                      </div>
                    </div>
                </div>
                @endfor
            </div>
            <br>
            <div class="row">
                @for($i=4;$i<8;$i++)
                <?php
                    $judul_view = $get_materi[$i]->judul;
                    if(strpos($judul_view,'Bab ') !== false){
                        $loc = strpos($judul_view,'Bab ')+4;
                        while($judul_view[$loc]!== ' '){
                            $loc+=1;
                        }
                        $judul_view = substr($judul_view,$loc+1);
                    }
                    $thumb_idx = strpos($get_materi[$i]->konten,'youtube.com/embed/')+18;
                    $len = 0;
                    while($get_materi[$i]->konten[$thumb_idx+$len] != '"'){
                        $len+=1;
                    }
                    $thumbnail = substr($get_materi[$i]->konten,$thumb_idx,$len);
                    $thumbnail = explode('?',$thumbnail);
                    $thumbnail = $thumbnail[0];
                ?>
                <div class="col-md-3">
                    <div class="card" style="width: 18rem;height: 100%">
                      <img src="https://img.youtube.com/vi/{{$thumbnail}}/hqdefault.jpg" class="card-img-top" alt="...">
                      <div class="card-body flex-column">
                        <h6 class="card-title">{{$judul_view}}</h6>
                        <br><br>
                        <p class="card-text mt-auto" style="position: absolute;bottom: 0px;right: 20px;"><i class="fa fa-eye" aria-hidden="true"></i> {{$get_materi[$i]->views}}</p>
                        <a href="{{site_url('materi/detail/'.$get_materi[$i]->id)}}" class="button button-black" style="position: absolute;bottom: 10px;">Detail</a>
                      </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
        <!-- Daftar kelas accessible -->
        <div class="job-categroy-area ptb-130 ptb-sm-60" style="padding-top: 0px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center ">
                            <h2>Topik yang tersedia</h2>
                        </div>
                    </div>
                </div>
                <div class="category-job-list-actiive owl-carousel">
                    @foreach($topik as $tp)
                    @if($tp->jumlah_materi)
                    <a href="<?php echo(site_url('materi?mapel='.$tp->id)); ?>">
                    <div class="category-item">
                        <div class="img-icon">
                            <img src="<?php base_url("jobhere/images/icons/cat1.png")?>" alt="">
                        </div>
                        <h5>{{$tp->nama}}</h5>
                        <h6>({{$tp->jumlah_materi}} Materi)</h6>
                    </div>
                    </a>
                    @endif  
                    @endforeach                  
                </div>
            </div>
        </div>
        <br><br>
        <br><br>
                               
    </div>
</div>
<!-- Working Aera End
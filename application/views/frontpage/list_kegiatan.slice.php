@extends('layouts.front.app')
@section('judul', ' Kegiatan Siswa')


@section('header')
@include('layouts.front.header')
@endsection

@section('content')
<div class="breadcrumb-banner-area pt-150 pb-85 bg-3" style="background:url('<?php echo base_url('assets/img/BackGround.png'); ?>') no-repeat scroll center top / cover;">
	<div style="background-color: rgba(85, 85, 85, 0.3);position: absolute;top: 0;left: 0;width: 100%;height: 100%;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-text">
                    <h2 class="text-center">Kegiatan Siswa</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://use.fontawesome.com/548ece7013.js"></script>
<!-- Daftar kelas accessible -->
<div class="woring-area pt-130 pb-100 pt-sm-60 pb-sm-30">
    <div class="container">
        <div class="job-post-area pb-100 pb-sm-35">
            <div class="row" id="result">
                @foreach($result as $hasil)
                <div class="title col-md-12">
                    <span>{{$hasil['topik']['nama']}}</span>
                </div>
                @foreach($hasil['jurnal'] as $jurnal)
                <div class="col-md-3">
                    <div class="card" style="width: 18rem;height: 100%">
                      <img src="{{$jurnal['instagram'].'media/?size=t'}}" class="card-img-top" alt="...">
                      <div class="card-body flex-column">
                        <h6 class="card-title">{{$jurnal['judul']}}</h6>
                        <br><br>
                        <?php
                        $get_nama = $this->siswa_model->retrieve($jurnal['siswa_id']);
                        ?>
                        <p class="card-text mt-auto" style="position: absolute;bottom: 0px;right: 20px;"><i class="fa fa-pen" aria-hidden="true"></i>{{$get_nama['nama']}}</p>
                        <a href="{{site_url('frontpage/detail_kegiatan/'.$jurnal['id'])}}" class="button button-black" style="position: absolute;bottom: 10px;">Detail</a>
                      </div>
                    </div>
                </div>
                @endforeach
                @endforeach
            </div>
            <br>
        </div>
    </div>
</div>
<div class="job-categroy-area pb-130 ptb-sm-60" style="padding-top: 0px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center ">
                    <h2>Topik Kegiatan</h2>
                </div>
            </div>
        </div>
        <div class="category-job-list-actiive owl-carousel">
            @foreach($topik as $tp)
            @if($tp->jumlah_materi)
            <a href="{{site_url('jurnal?mapel='.$tp->id)}}">
            <div class="category-item">
                <div class="img-icon">
                    <img src="<?php base_url("jobhere/images/icons/cat1.png")?>" alt="">
                </div>
                <h5>{{$tp->nama}}</h5>
                <h6>({{$tp->jumlah_materi}} Kegiatan)</h6>
            </div>
            </a>
            @endif  
            @endforeach                  
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('layouts.front.footer')
@endsection

@section('moreJS')
<script>
// function cari(string){
// 	$.ajax({
// 		url:"{{site_url('frontpage/cariAction')}}",
// 		method:"POST",
// 		data:{keyword:string},
// 		success:function(data){
// 			console.log(data);
// 			$('#result').html(data);
// 		}
// 	})
// }
// $(document).ready(function(){
// 	cari();
// 	$('#cari').on('submit',function(e){
// 		e.preventDefault();
// 		var search = $('#cari_field').val();
// 		if(search != '')
// 		{
// 			cari(search);
// 		}
// 		else
// 		{
// 			cari();
// 		}
// 	});
// });
</script>
@endsection
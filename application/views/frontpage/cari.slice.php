@extends('layouts.front.app')
@section('judul', ' Cari')


@section('header')
@include('layouts.front.header')
@endsection

@section('content')
<div class="breadcrumb-banner-area pt-150 pb-85 bg-3" style="background:url('<?php echo base_url('assets/img/BackGround.png'); ?>') no-repeat scroll center top / cover;">
	<div style="background-color: rgba(85, 85, 85, 0.3);position: absolute;top: 0;left: 0;width: 100%;height: 100%;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-text">
                    <h2 class="text-center">Cari Materi</h2>
                </div>
                <div class="job-search-content text-center brd-style search-container">
                    <form action="" method="post" id="cari">
                        <input type="text" placeholder="Cari Materi" id="cari_field">
                        <button>Search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://use.fontawesome.com/548ece7013.js"></script>
<!-- Daftar kelas accessible -->
<div class="woring-area pt-130 pb-100 pt-sm-60 pb-sm-30">
    <div class="container">
        <div class="job-post-area pb-100 pb-sm-35">
            <div class="row" id="result">
                
            </div>
            <br>
        </div>
    </div>
</div>
<div class="job-categroy-area pb-130 ptb-sm-60" style="padding-top: 0px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center ">
                    <h2>Topik yang tersedia</h2>
                </div>
            </div>
        </div>
        <div class="category-job-list-actiive owl-carousel">
            @foreach($topik as $tp)
            @if($tp->jumlah_materi)
            <a href="<?php echo(site_url('materi?mapel='.$tp->id)); ?>">
            <div class="category-item">
                <div class="img-icon">
                    <img src="<?php base_url("jobhere/images/icons/cat1.png")?>" alt="">
                </div>
                <h5>{{$tp->nama}}</h5>
                <h6>({{$tp->jumlah_materi}} Materi)</h6>
            </div>
            </a>
            @endif  
            @endforeach                  
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('layouts.front.footer')
@endsection

@section('moreJS')
<script>
function cari(string){
	$.ajax({
		url:"{{site_url('frontpage/cariAction')}}",
		method:"POST",
		data:{keyword:string},
		success:function(data){
			console.log(data);
			$('#result').html(data);
		}
	})
}
$(document).ready(function(){
	cari();
	$('#cari').on('submit',function(e){
		e.preventDefault();
		var search = $('#cari_field').val();
		if(search != '')
		{
			cari(search);
		}
		else
		{
			cari();
		}
	});
});
</script>
@endsection
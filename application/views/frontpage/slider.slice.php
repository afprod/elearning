<!--Start of Slider Area-->
<div class="slider-area">
    <div class="preview-2">
        <div id="contain-slide">
            <div class="slider" style="z-index: 99;">
                <div class="track" data-glide-el="track">
                    <ul class="slides">
                        <li class="slide"><div class="img" style="background-image:url('http://smadt.net/wp-content/uploads/slider5/Group19.png');"></div></li>
                        <li class="slide"><div class="img" style="background-image:url('http://smadt.net/wp-content/uploads/slider5/Group34.png');"></div></li>
                        <li class="slide"><div class="img" style="background-image:url('http://smadt.net/wp-content/uploads/slider5/Group32.png');"></div></li>
                        <li class="slide"><div class="img" style="background-image:url('http://smadt.net/wp-content/uploads/slider5/Group33.png');"></div></li>
                        <li class="slide"><div class="img" style="background-image:url('http://smadt.net/wp-content/uploads/slider5/Group35.png');"></div></li>
                        <li class="slide"><div class="img" style="background-image:url('http://smadt.net/wp-content/uploads/slider5/Group16.png');"></div></li>
                        <li class="slide"><div class="img" style="background-image:url('http://smadt.net/wp-content/uploads/slider5/Group30.png');"></div></li>
                    </ul>
                </div>
                <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow glide__arrow--left" data-glide-dir="<"><</button>
                    <button class="glide__arrow glide__arrow--right" data-glide-dir=">">></button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Slider Area-->
@section('moreJS')
<!-- Required Core Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/glide/dist/css/glide.core.min.css');?>">
<!-- Optional Theme Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url('assets/glide/dist/css/glide.theme.min.css');?>">
<style type="text/css">
    /*@media (min-width: @screen-sm-min) { ... }*/

    /* Medium devices (desktops, 992px and up) */
    /*@media (min-width: @screen-md-min) { ... }*/

    /* Large devices (large desktops, 1200px and up) */
    /*@media (min-width: @screen-lg-min) { ... }*/
    @media (min-width: 1025px) {
        #contain-slide{
            height: 100vh;
            padding-top: 8% ;
            background-image : url("<?php echo base_url('assets/img/BackGround.png'); ?>");
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    }
    @media (max-width: 1024px){
        #contain-slide{
            height: 44vh;
            padding-top: 8% ;
            background-image : url("<?php echo base_url('assets/img/BackGround.png'); ?>");
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    }
    /*.slider-area:before{
        background: rgba(255, 255, 255, 0.5);
    }

    .your-class{
        height: 90%;
        width: 80%;
        margin: auto;
        padding-top: 8%; 
    }
    .slider-content img{
        height: 75vh;
        width: auto;
    }*/
</style>
<!-- <style type="text/css">
    .preview-2 img{
        height : 100vh;
        width: 100%;
        object-fit:cover;

        background-position: center;
        background-size: cover;
    }
</style> -->

<style type="text/css">
    @media (min-width: 1025px) {
        .slider {
          position: relative;
          width: 100%;
          height: 80vh;
          max-width:1000px;
          overflow: hidden;
          margin: auto;
        }
        .img {
            position: relative;
            float: left;
            width: 100%;
            height: 70vh;
            background-position: 50% 50%;
            background-repeat:   no-repeat;
            background-size:     cover;
        }
    }
    @media (max-width: 1024px) {
        .slider {
          position: relative;
          width: 100%;
          height: 50vh;
          max-width:600px;
          overflow: hidden;
          margin: auto;
        }
        .img {
            position: relative;
            float: left;
            width: 100%;
            height: 30vh;
            background-position: 50% 50%;
            background-repeat:   no-repeat;
            background-size:     cover;
        }
    }
     
    .slides {
      height: 100%;
      overflow: hidden;
      /**   
       * Prevent blinking issue
       * Not tested. Experimental.
       */
      -webkit-backface-visibility: hidden;
      -webkit-transform-style: preserve-3d;
     
       -webkit-transition: all 500ms cubic-bezier(0.165, 0.840, 0.440, 1.000); 
       -moz-transition: all 500ms cubic-bezier(0.165, 0.840, 0.440, 1.000); 
       -ms-transition: all 500ms cubic-bezier(0.165, 0.840, 0.440, 1.000); 
       -o-transition: all 500ms cubic-bezier(0.165, 0.840, 0.440, 1.000); 
       transition: all 500ms cubic-bezier(0.165, 0.840, 0.440, 1.000);
    }
    .slide {
      height: 100%;
      float: left;
      clear: none;
    }
    .slide figure {
      display: block;
      position: relative;
      text-align: center;
    }
    .slide figure figcaption {
      position: absolute;
      right: 20%;
      font-size: 1.1em;
      font-weight: bold;
      padding: 8px 14px;
      color: #464646;
      background: rgba(255,255,255,0.8); 
    }
    .slide figure figcaption a {
      color: #5a7fbc;
      text-decoration: none;
    }
    .slide figure figcaption a:hover { text-decoration: underline; }
     
    .slide figure img {
      max-height: 480px;
    }
    .slider-arrows {}
     
    .slider-arrow {
      position: absolute;
      display: block;
      margin-bottom: -20px;
      padding: 20px;
      font-family: 'Alegreya Sans', 'Trebuchet MS', sans-serif;
      text-decoration: none;
      font-weight: 900;
      font-size: 3.0em;
      color: #fff;
      border: 1px solid #fff;
      border-radius: 8px;
      -webkit-transition: all 0.2s linear;
      -moz-transition: all 0.2s linear;
      transition: all 0.2s linear;
    }
    .slider-arrow:hover {
      background: #ddd;
      color: #aaa;
    }
    /*.slider-arrow--right { bottom: 50%; right: 30px; }
    .slider-arrow--left { bottom: 50%; left: 30px; }*/
     
     
     
    .slider-nav {
      position: absolute;
      bottom: 0px;
    }
     
    .slider-nav__item {
      width: 12px;
      height: 12px;
      float: left;
      clear: none;
      display: block;
      margin: 0 5px;
      background: #fff;
      -webkit-border-radius: 7px;
      -moz-border-radius: 7px;
      border-radius: 7px;
    }
    .slider-nav__item:hover { background: #bababa; }
    .slider-nav__item--current, .slider-nav__item--current:hover { background: #999; }
</style>
<script type="text/javascript" src="<?php echo base_url('assets/glide/dist/glide.min.js'); ?>"></script>
<script>
    new Glide('.slider',{
        autoplay: 3500,
        hoverpause: true, // set to false for nonstop rotate
        arrowRightText: '&rarr;',
        arrowLeftText: '&larr;'
    }).mount();
</script>
@endsection
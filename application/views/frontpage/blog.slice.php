<!--Start of Blog Area-->
<div class="blog-area pb-100 pt-sm-60 pb-sm-30">
    <div class="container">
        <!-- Section Title Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center ">
                    <a target="_blank" href="{{ base_url('frontpage/ruangbaca') }}"> <h2>Ruang Baca</h2> </a>
                    <p>Baca berita terkini dari ruang baca</p>
                </div>
            </div>
        </div>
        <!-- Section Title End -->
        <div class="blog-carousel carousel-style-two">
            <!-- Single Item Start -->
            <div class="single-blog hover-effect">
                <div class="blog-image box-hover">
                    <a href="#" class="block">
                        <img src="{{ base_url('jobhere/images/blog/1.jpg') }}" alt="">
                    </a>
                </div>
                <div class="blog-text">
                    <h4><a href="#">Berita Terkini</a></h4>
                    <div class="blog-post-info">
                        <span>Feb 13.</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adiicing elit, sed do eiusmod tempor incididunt ut labore et dolor magna aliqua. Ut enim</p>
                </div>
            </div>
            <!-- Single Item End -->
            <!-- Single Item Start -->
            <div class="single-blog hover-effect">
                <div class="blog-image box-hover">
                    <a href="#" class="block">
                        <img src="{{ base_url('jobhere/images/blog/2.jpg') }}" alt="">
                    </a>
                </div>
                <div class="blog-text">
                    <h4><a href="#">Manfaat Budi Daya Jamur</a></h4>
                    <div class="blog-post-info">
                        <span>Feb 13.</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adiicing elit, sed do eiusmod tempor incididunt ut labore et dolor magna aliqua. Ut enim</p>
                </div>
            </div>
            <!-- Single Item End -->
            <!-- Single Item Start -->
            <div class="single-blog hover-effect">
                <div class="blog-image box-hover">
                    <a href="#" class="block">
                        <img src="{{ base_url('jobhere/images/blog/3.jpg') }}" alt="">
                    </a>
                </div>
                <div class="blog-text">
                    <h4><a href="#">Jurnal Ilmiah Tentang AA</a></h4>
                    <div class="blog-post-info">
                        <span>Feb 13.</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adiicing elit, sed do eiusmod tempor incididunt ut labore et dolor magna aliqua. Ut enim</p>
                </div>
            </div>
            <!-- Single Item End -->
        </div>
    </div>
</div>
<!--End of Blog Area-->
@extends('layouts.front.app')
@section('judul', ' Kontak')

@section('header')
    @include('layouts.front.header')
@endsection

@section('content')
<br><br><br><br><br><br>
<div class="breadcrumb-banner-area pt-94 pb-85 bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-text">
                    <h2 class="text-center">Kontak Kami</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Breadcrumb Banner Area-->
<!--Start of Google Map-->
<div class="ptb-130 ptb-sm-60">
    <!--Start of Contact Form And info-->
    <div class="container">
        <div class="gray-bg fix">
            <div class="col-4 light-gray-bg contact-left">
                <div class="contact-info text-center">
                    <a href="mailto:dt.doubletrack@gmail.com" target="_blank">
                        <div class="single-contact-info">
                            <div class="single-contact-icon">
                                <i class="zmdi zmdi-email"></i>
                            </div>
                            <div class="single-contact-text">
                                <span class="block" id="kontak">dt.doubletrack@gmail.com</span>
                            </div>
                        </div>
                    </a> 
                    <a href="https://api.whatsapp.com/send?phone=6281230269120">
                        <div class="single-contact-info">
                            <div class="single-contact-icon">
                                    <i class="zmdi zmdi-phone"></i>
                            </div>
                            <div class="single-contact-text">
                                    <span class="block" id="kontak">+6281 230 269 120 </span>
                            </div>
                        </div>
                    </a>
                    <div class="single-contact-info">
                        <div class="single-contact-icon">
                            <i class="zmdi zmdi-pin"></i>
                        </div>
                        <div class="single-contact-text">
                            <span class="block"><p>UPT Pusat Pelatihan dan Sertifikasi Profesi ITS Gedung Research Center ITS, Lantai 3 Kampus ITS Sukolilo, Surabaya 601111</p></span>
                        </div>
                    </div>
                </div>
                <div class="followus">
                    <h4>Follow Us</h4>
                    <div class="social-links">
                        <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                        <a href="#"><i class="zmdi zmdi-rss"></i></a>
                        <a href="#"><i class="zmdi zmdi-google-plus"></i></a>
                        <a href="#"><i class="zmdi zmdi-pinterest"></i></a>
                        <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-8 contact-right">
                <div class="contact-form pt-60 pb-30 fix">
                    <h4>Kirim Pesan</h4>
                    <form method="post" action="<?php echo base_url('PesanController/add'); ?>">
                        <div class="col-5 pr-6">
                            <input required="" type="text" name="nama" id="name" class="pl-20" placeholder="Name">
                        </div>
                        <div class="col-5 pl-6">
                            <input required="" type="text" name="email" id="email" class="pl-20" placeholder="Email">
                        </div>
                        <div class="col-10">
                            <input type="text" name="subject" id="subject" class="pl-20" placeholder="Subject">
                        </div>
                        <div class="col-10">
                            <textarea required="" name="isi" id="isi" cols="30" rows="10" placeholder="Message" class="mb-10"></textarea>
                        </div>
                        <div class="col-10 fix">
                            <button type="submit" class="button submit-btn">SUBMIT</button>
                        </div>
                        <p class="form-messege"></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('layouts.front.footer')
@endsection
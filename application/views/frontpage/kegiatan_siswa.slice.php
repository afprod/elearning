@extends('layouts.front.app')
@section('judul', ' Kegiatan Saya')


@section('header')
@include('layouts.front.header')
@endsection

@section('content')
<div class="breadcrumb-banner-area pt-150 pb-85 bg-3" style="background:url('<?php echo base_url('assets/img/BackGround.png'); ?>') no-repeat scroll center top / cover;">
	<div style="background-color: rgba(85, 85, 85, 0.3);position: absolute;top: 0;left: 0;width: 100%;height: 100%;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-text">
                    <h2 class="text-center">Kegiatan Saya</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="canditates-area pt-50 pb-130 pb-sm-60">
    <div class="container">
        <div class="all-job-post">
        	<div class="row pb-50">
        		<div class="col-md-12">
                    <div class="loadmorebtn">
                        <a href="{{site_url('jurnal/tambah')}}" class="button">Tambah Kegiatan Baru</a>
                    </div>
                </div>
        	</div>
            <div class="row">
            	@foreach($jurnal as $jur)
                <div class="col-md-6">
                    <div class="single-job-post">
                         <div class="img-icon">
                            <img src="{{$jur['instagram'].'media/?size=t'}}" alt="">
                        </div>
                        <div class="address">
                            <h6>{{$jur['judul']}}</h6>
                            <p><span>{{$jur['tanggal']}}</span></p>
                            <p>{{$jur['mapel']}}</p>
                        </div>
                        <div class="button-box"><a href="{{site_url('frontpage/detail_kegiatan/'.$jur['id'])}}" class="button button-black">Lihat Jurnal</a></div>
                    </div>                                    
                </div>
                @endforeach
                <div class="col-md-12">
                    <div class="loadmorebtn">
                        <a href="{{site_url('jurnal/index')}}" class="button">Muat Lebih</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('layouts.front.footer')
@endsection

@section('moreJS')
<script>

</script>
@endsection

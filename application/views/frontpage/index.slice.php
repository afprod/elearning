@extends('layouts.front.app')
@section('judul', ' Homepage')

@section('header')
    @include('layouts.front.header')
@endsection

@section('content')
    @include('frontpage.slider')

    @include('frontpage.alur')

@endsection

@section('footer')
    @include('layouts.front.footer')
@endsection
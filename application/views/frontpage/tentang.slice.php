@extends('layouts.front.app')
@section('judul', ' Tentang')

@section('header')
    @include('layouts.front.header')
@endsection

@section('content')
<br><br>
<br><br>
<br><br>
<div class="breadcrumb-banner-area pt-94 pb-85 bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-text">
                    <h2 class="text-center">Tentang Dual Track</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Breadcrumb Banner Area-->
<!-- About Area Start -->
<div class="about-area ptb-130 ptb-sm-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="abt-img img-full">
                    <img src="{{ base_url('assets/img/logo_dual_track.png') }}" alt="jobhere">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="abt-content">
                    <h4><span>Apa itu</span> Dual Track?</h4>
                    <p><strong>SMA Dual Track (SMA-DT)</strong> adalah SMA yang melaksanakan kegiatan KBM reguler dan
                        menyelenggarakan kegiatan pembekalan keterampilan secara berdampingan dengan
                        memanfaatkan kearifan lokal.</p><br>
                    <p><strong>1. Daftar</strong> <br>
                        Untuk mengikuti program Dual Track Sekolah yang ditunjuk mengisi pendaftaran 
                        siswa kemudian memilih Rombongan Belajar sesuai dengan bidang keahlian yang diminati.
                        Semua proses pendaftaran ada di <a href="https://admindt.net">www.admindt.net</a>.
                    </p>

                    <p><strong>2. Training</strong> <br>
                        Siswa peserta program Dual Track mengikuti kegiatan training dan praktek di sekolah-sekolah
                        yang ditunjuk, dan membekali pengetahuan melalui modul-modul yang tersedia dalam bentuk
                        media cetak, ebook, maupun tutorial video multimedia yang tersedia di 
                        <a href="https://ruangtraining.net">www.ruangtraining.net</a>.
                    </p>

                    <p><strong>3. Sertifikasi</strong> <br>
                        Setelah siswa dibekali pengetahuan dan ketrampilan, tahapan berikutnya para siswa peserta
                        Program Dual Track dapat mengikuti proses sertifikasi melalui serangkaian ujian yang ada di
                        <a href="https://ruangujian.net">www.ruangujian.net</a>.
                    </p>

                    <p><strong>4. Kerja</strong> <br>
                        Siswa yang sudah menyelesaikan program pelatihan dan lulus dari ujian dapat memasuki jenjang
                        pekerjaan yang ada di <a href="https://ruangkarir.net">www.ruangkarir.net</a>.
                        Di aplikasi ini Rekan DUDI, Dunia Usaha dan Dunia Industri akan memberikan informasi mengenai
                        profil pekerjaan yang tersedia dan juga spesifikasi kemampuan calon pekerja yang dibutuhkan.

                    </p> 
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<!-- Section Title Start -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="section-title text-center ">
                <h2>Bagaimana Alur Pendaftaran?</h2>
                <p>Lakukan 4 langkah berikut.</p>
            </div>
        </div>
    </div>
    <!-- Section Title End -->
    <div class="row work-shap">
        <div class="col-md-3">
            <div class="work-item">
                <div class="img-icon">
                    <img src="{{ base_url('assets/jobhere/images/icons/wrk1.png') }}" alt="">
                </div>
                <h5>Pendaftaran</h5>
                <p>Semua proses pendaftaran dilakukan oleh pihak sekolah ada di <a href="https://admindt.net/">www.admindt.net</a></p>
            </div> 
        </div>
        <div class="col-md-3">
            <div class="work-item">
                <div class="img-icon">
                    <img src="{{ base_url('assets/jobhere/images/icons/wrk2.png') }}" alt="">
                </div>
                <h5>Training</h5>
                <p>Segala tutorial untuk menunjang pembelajaran siswa ada di <a href="https://ruangtraining.net">www.ruangtraining.net</a></p>
            </div> 
        </div>
        <div class="col-md-3">
            <div class="work-item">
                <div class="img-icon">
                    <img src="{{ base_url('assets/jobhere/images/icons/wrk2.png') }}" alt="">
                </div>
                <h5>Sertifikasi</h5>
                <p>Proses sertifikasi melalui serangkaian ujian yang ada di <a href="https://ruangujian.net">www.ruangujian.net</a></p>
            </div> 
        </div>
        <div class="col-md-3">
            <div class="work-item">
                <div class="img-icon">
                    <img src="{{ base_url('assets/jobhere/images/icons/wrk3.png') }}" alt="">
                </div>
                <h5>Kerja</h5>
                <p>Peserta yang menyelesaikan proses akan terdata di <a href="https://ruangkarir.net">www.ruangkarir.net</a></p>
            </div> 
        </div>
    </div>
</div>
<br><br>
<br><br>
<br><br>
<br><br>
@endsection

@section('footer')
    @include('layouts.front.footer')
@endsection
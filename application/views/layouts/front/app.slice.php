<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Ruang Training | @yield('judul')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- favicon
        ============================================ -->        
        <link rel="shortcut icon" type="image/x-icon" href="https://ruangtraining.net/img/favicon.ico">
        
        <!-- Google Fonts
        ============================================ -->        
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
        
        <!-- All css files are included here
        ============================================ -->    
        <!-- Bootstrap CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/jobhere/css/bootstrap.min.css'); ?>">
        
        <!-- This core.css file contents all plugins css file
        ============================================ -->
        <!-- Nice select css
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/jobhere/css/nice-select.css'); ?>">
        <!-- This core.css file contents all plugins css file
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/jobhere/css/core.css'); ?>">
        
        <!-- Theme shortcodes/elements style
        ============================================ -->  
        <link rel="stylesheet" href="<?php echo base_url('assets/jobhere/css/shortcode/shortcodes.css'); ?>">
        
        <!--  Theme main style
        ============================================ -->  
        <link rel="stylesheet" href="<?php echo base_url('assets/jobhere/style.css'); ?>">
        
        <!-- Color CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/jobhere/css/plugins/color.css'); ?>">
        
        <!-- Responsive CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/jobhere/css/responsive.css'); ?>">
        
        <!-- Modernizr JS -->
        <script src="<?php echo base_url('assets/jobhere/js/vendor/modernizr-2.8.3.min.js'); ?>"></script>
        <style>
        @media only screen and (max-width: 600px) {
            .logo-img{
                width: 100% !important;
            }
        }
        #kontakfooter:hover {
            color: blue;
        }
        #kontakfooter{
            color: #fff;
        }
        #kontak:hover {
            color: blue;
        }
        #kontak{
            color: #4c4c4c;
        }
        </style>
        
    </head>  
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->  

        <!--Main Wrapper Start-->
        <div class="as-mainwrapper">
            <!--Bg White Start-->
            <div class="bg-white">
                <!--Header Area Start-->
                @yield('header')
                <!-- End of Header Area -->
                @yield('content')
                @yield('slider')
                @yield('search')
                @yield('job')
                @yield('alur')
                @yield('blog')
                <!--Start of Footer Widget-area-->
                @yield('footer')

                <!-- End of Footer area -->
            </div>   
            <!--End of Bg White--> 
        </div>    
        <!--End of Main Wrapper Area--> 
            
        <!--Start of Login Form-->
        <div id="quickview-login">
            <!-- Modal -->
            <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-pop-up-content ptb-60 pl-60 pr-60">
                                <div class="area-title text-center mb-43">
                                    <img src="{{ base_url('assets/jobhere/images/logo/logo.png')}}" alt="jobhere">
                                </div>
                                <form method="post" action="#">
                                    <div class="form-box">
                                        <input type="text" name="username" placeholder="Email" class="mb-14">
                                        <input type="password" name="pass" placeholder="Password">
                                    </div>
                                    <div class="fix ptb-30">
                                        <span class="remember pull-left"><input class="p-0 pull-left" type="checkbox">Remember Me</span>
                                        <span class="pull-right"><a href="#">Forget Password?</a></span>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="text-uppercase">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>  
                </div>
            </div>
        </div>
        <!--End of Login Form-->
        <!--Start of Login Form-->
        <div id="quickview-register">
            <!-- Modal -->
            <div class="modal fade" id="register" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-pop-up-content ptb-60 pl-60 pr-60">
                                <div class="area-title text-center mb-43">
                                    <img src="{{ base_url('assets/jobhere/images/logo/logo.png')}}" alt="jobhere">
                                </div>
                                <form method="post" action="#">
                                    <div class="form-box box2">
                                        <input type="text" name="firstname" placeholder="First Name" class="mb-14">
                                        <input type="text" name="lastname" placeholder="Last Name">
                                    </div>
                                    <div class="form-box">
                                        <input type="email" name="emailnew" placeholder="Email" class="mb-14">
                                        <input type="password" name="pass" placeholder="Password">
                                    </div>
                                    <div class="fix ptb-30">
                                        <span class="remember pull-left"><input class="p-0 pull-left" type="checkbox">Remember Me</span>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="text-uppercase">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div>  
                </div>
            </div>
        </div>
        <!--End of Login Form-->
       
        
        <!-- jquery latest version
        ========================================================= -->
        <script src="<?php echo base_url('assets/jobhere/js/vendor/jquery-1.12.4.min.js'); ?>"></script>
        
        <!-- Bootstrap framework js
        ========================================================= -->          
        <script src="<?php echo base_url('assets/jobhere/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/jobhere/js/popper.js'); ?>"></script>
        
        <!-- Owl Carousel js
        ========================================================= -->       
        <script src="<?php echo base_url('assets/jobhere/js/owl.carousel.min.js'); ?>"></script>
            
        <!-- Jquery nice select js 
        ========================================================= -->   
        <script src="<?php echo base_url('assets/jobhere/js/jquery.nice-select.min.js'); ?>"></script>
        
        <!-- nivo slider js
        ========================================================= -->       
        <script src="<?php echo base_url('assets/jobhere/lib/nivo-slider/js/jquery.nivo.slider.js'); ?>"></script>
        <script src="<?php echo base_url('assets/jobhere/lib/nivo-slider/home.js'); ?>"></script>
        
        <!-- Js plugins included in this file
        ========================================================= -->   
        <script src="<?php echo base_url('assets/jobhere/js/plugins.js'); ?>"></script>
        
        <!-- Video Player JS
        ========================================================= -->           
        <script src="<?php echo base_url('assets/jobhere/js/jquery.mb.YTPlayer.js'); ?>"></script>
        
        <!-- AJax Mail JS
        ========================================================= -->           
        <script src="<?php echo base_url('assets/jobhere/js/ajax-mail.js'); ?>"></script>
        
        <!-- Mail Chimp JS
        ========================================================= -->           
        <script src="<?php echo base_url('assets/jobhere/js/jquery.ajaxchimp.min.js'); ?>"></script>
        
        <!-- Waypoint Js
        ========================================================= -->   
        <script src="<?php echo base_url('assets/jobhere/js/waypoints.min.js'); ?>"></script>
        
        <!-- Main js file contents all jQuery plugins activation
        ========================================================= -->       
        <script src="<?php echo base_url('assets/jobhere/js/main.js'); ?>"></script>
        
        @yield('moreJS')

    </body>
</html>
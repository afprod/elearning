<header class="header-area sticky">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="logo"><a href="{{ base_url('/') }}"><img class="logo-img" src="http://smadt.net/wp-content/uploads/2019/08/logolan1.png" alt="JobHere"></a></div>
            </div>
            <div class="col-lg-9 d-none d-lg-block">
                <div class="pull-right">
                    <nav id="primary-menu">
                        <ul class="main-menu text-right">
                            <li><a class="
                                @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'index') text-success @endif
                                " href="{{ site_url('frontpage/index') }}">Home</a>
                            </li>
                            <li><a class="
                                @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'cari') text-success @endif
                                " href="{{ site_url('frontpage/cari') }}">Cari</a></li>
                            <li><a class="
                                @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'kegiatan') text-success @endif
                                " href="{{ site_url('frontpage/kegiatan') }}">Kegiatan Siswa</a></li>
                            <!-- <li><a class="
                            @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'sekolah') text-success @endif
                            " href="{{ site_url('frontpage/sekolah') }}">Sekolah</a></li> -->
                            <li><a href="#" class="
                                @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'ruangbaca') text-success @endif
                                ">Ruang</a>
                                <ul class="dropdown">
                                    <!-- <li><a  href="{{ base_url('frontpage/ruangbaca') }}">Ruang Baca</a></li> -->
                                    <li><a target="_blank" href="https://ruangujian.net/">Ruang Ujian</a></li>
                                    <li><a target="_blank" href="https://ruangkarir.net/">Ruang Karir</a></li>
                                    <li><a target="_blank" href="https://ruangdagang.net/">Ruang Dagang</a></li>
                                </ul>
                            </li>
                            <li><a class="
                                @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'tentang') text-success @endif
                                " href="{{ site_url('frontpage/tentang') }}">Tentang</a></li>
                            <li><a class="
                                @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'kontak') text-success @endif
                                " href="{{ site_url('frontpage/kontak') }}">Kontak</a></li>
                            @if(is_login())
                            <li><a href="#">{{nama_panggilan(get_sess_data('user', 'nama'))}}</a>
                                <ul class="dropdown">
                                    <li><a href="{{ site_url('welcome/index') }}">RuangTraining</a></li>
                                    @if(is_admin())
                                    <li><a target="_blank" href="{{site_url('pengajar/detail/'.get_sess_data('user', 'status_id').'/'.get_sess_data('user', 'id'))}}">Detail Profil</a></li>
                                    @endif

                                    @if(is_pengajar())
                                    <li><a href="{{site_url('login/pp')}}">Profil & Akun Login</a></li>
                                    @endif

                                    @if(is_siswa())
                                    <li><a href="{{site_url('login/pp')}}">Profil & Akun Login</a></li>
                                    <li><a href="{{site_url('frontpage/kegiatan_siswa')}}">Kegiatan Saya</a></li>
                                    @endif
                                    <li><a href="{{ site_url('login/logout') }}">Logout</a></li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </nav>
                    @if(!is_login())
                    <div class="login-btn">
                        <!-- @if(in_array($this->session->user_login['role'], ['a01']))
                            <a href="{{ base_url('dashboard') }}" class="modal-view button"> <i class="fa fa-caret-square-o-right"></i> Dashboard</a>
                            <a href="{{ base_url('Sessions/logout') }}" class="modal-view button">
                                <i class="fa fa-caret-square-o-left"></i> Logout
                            </a>
                        @elseif(in_array($this->session->user_login['role'], ['s02']))
                            <a href="{{ base_url('dashboard') }}" class="modal-view button"> <i class="fa fa-caret-square-o-left"></i> Dashboard</a>
                            <a href="{{ base_url('Sessions/logout') }}" class="modal-view button">
                                <i class="fa fa-caret-square-o-left"></i> Logout
                            </a>
                        @else -->
                            <!-- <a href="{{ base_url('auth/index') }}" class="modal-view button">Masuk</a> -->
                            <a href="{{ site_url('login/index') }}" class="modal-view button">Masuk</a>
                        <!-- @endif -->
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu Area start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a class="
                                    @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'index') text-success @endif
                                    " href="{{ site_url('frontpage/index') }}">Home</a>
                                </li>
                                <li><a class="
                                    @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'cari') text-success @endif
                                    " href="{{ site_url('frontpage/cari') }}">Cari</a></li>
                                <li><a class="
                                    @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'kegiatan') text-success @endif
                                    " href="{{ site_url('frontpage/kegiatan') }}">Kegiatan Siswa</a></li>
                                <!-- <li><a href="job-board.html">Pekerjaan</a>
                                    <ul class="sub-menu">
                                        <li><a href="">Single Job</a></li>
                                        <li><a href="">Job Details</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="about.html">Tentang</a></li>
                                <li><a href="contact.html">Kontak</a></li>
                                <li><a class="modal-view button" href="#" data-toggle="modal" data-target="#register">Daftar</a></li>
                                <li><a class="modal-view button" href="#" data-toggle="modal" data-target="#productModal">Masuk</a></li> -->
                                <li><a href="#" class="
                                    @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'ruangbaca') text-success @endif
                                    ">Ruang</a>
                                    <ul id="dropdown">
                                        <!-- <li><a  href="{{ base_url('frontpage/ruangbaca') }}">Ruang Baca</a></li> -->
                                        <li><a target="_blank" href="https://ruangujian.net/">Ruang Ujian</a></li>
                                        <li><a target="_blank" href="https://ruangkarir.net/">Ruang Karir</a></li>
                                        <li><a target="_blank" href="https://ruangdagang.net/">Ruang Dagang</a></li>
                                    </ul>
                                </li>
                                <li><a class="
                                    @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'tentang') text-success @endif
                                    " href="{{ site_url('frontpage/tentang') }}">Tentang</a></li>
                                <li><a class="
                                    @if($this->router->fetch_class() == 'frontpage' && $this->router->fetch_method() == 'kontak') text-success @endif
                                    " href="{{ site_url('frontpage/kontak') }}">Kontak</a></li>
                                 @if(is_login())
                                <li><a href="#">{{nama_panggilan(get_sess_data('user', 'nama'))}}</a>
                                    <ul id="dropdown">
                                        <li><a href="{{ site_url('welcome/index') }}">RuangTraining</a></li>
                                        @if(is_admin())
                                        <li><a target="_blank" href="{{site_url('pengajar/detail/'.get_sess_data('user', 'status_id').'/'.get_sess_data('user', 'id'))}}">Detail Profil</a></li>
                                        @endif

                                        @if(is_pengajar())
                                        <li><a href="{{site_url('login/pp')}}">Profil & Akun Login</a></li>
                                        @endif

                                        @if(is_siswa())
                                        <li><a href="{{site_url('frontpage/kegiatan_siswa')}}">Kegiatan Saya</a></li>
                                        <li><a href="{{site_url('login/pp')}}">Profil & Akun Login</a></li>
                                        @endif
                                        <li><a href="{{ site_url('login/logout') }}">Logout</a></li>
                                    </ul>
                                </li>
                                @else
                                <li>
                                    <!-- <div>
                                        @if(in_array($this->session->user_login['role'], ['a01']))
                                            <a href="{{ base_url('dashboard') }}" class="modal-view button"> <i class="fa fa-caret-square-o-right"></i> Dashboard</a>
                                            <a href="{{ base_url('Sessions/logout') }}" class="modal-view button">
                                                <i class="fa fa-caret-square-o-left"></i> Logout
                                            </a>
                                        @elseif(in_array($this->session->user_login['role'], ['s02']))
                                            <a href="{{ base_url('dashboard') }}" class="modal-view button"> <i class="fa fa-caret-square-o-left"></i> Dashboard</a>
                                            <a href="{{ base_url('Sessions/logout') }}" class="modal-view button">
                                                <i class="fa fa-caret-square-o-left"></i> Logout
                                            </a>
                                        @else -->
                                            <a href="{{ site_url('login/index') }}" >Masuk</a>
                                        <!-- @endif -->
                                    </div>
                                </li>
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu Area end -->
</header>
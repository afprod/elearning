<footer class="footer-area">
    <div class="footer-widget-area ptb-80 pb-sm-30 bg-secondary">
        <div class="container">
            <div class="row">
                <div class="col-lg-1 col-md-5"></div>
                <div class="col-lg-4 col-md-5">
                    <div class="single-footer-widget">
                        <h3 class="text-white mb-26">Hubungi Kami</h3>
                        <span class="text-white mb-9"><a href="https://api.whatsapp.com/send?phone=6281230269120" id="kontakfooter"><i class="fa fa-phone"></i>+6281 230 269 120 </a></span>
                        <span class="text-white mb-9"><a href="mailto:dt.doubletrack@gmail.com" target="_blank" id="kontakfooter"><i class="fa fa-envelope"></i>dt.doubletrack@gmail.com</a></span>
                        <span class="text-white mb-9"><i class="fa fa-globe"></i>www.ruangtraining.net</span>
                        <span class="text-white mb-9"><i class="fa fa-map-marker"></i><p>UPT Pusat Pelatihan dan Sertifikasi Profesi ITS <br>Gedung Research Center ITS, Lantai 3 <br>Kampus ITS Sukolilo, Surabaya 601111</p></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Footer Widget-area-->
    <!-- Start of Footer area -->
    <div class="copyright-area text-center ptb-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-text">
                        <span class="text-white block">Copyright&copy; <a href="https://ruangtraining.net">Dual Track</a>. All right reserved.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
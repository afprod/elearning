<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ruang Training | @yield('judul')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url('adminlte/plugins/daterangepicker/daterangepicker-bs3.css');?>">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo base_url('adminlte/plugins/iCheck/all.css');?>">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="<?php echo base_url('adminlte/plugins/colorpicker/bootstrap-colorpicker.min.css');?>">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?php echo base_url('adminlte/plugins/timepicker/bootstrap-timepicker.min.css');?>">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url('adminlte/plugins/select2/select2.min.css');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('adminlte/dist/css/adminlte.min.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
    <!-- List and Grid -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

    <!-- Modal -->
    <!-- <link rel="stylesheet" href="https://cdn.rawgit.com/JacobLett/bootstrap4-latest/master/bootstrap-4-latest.min.css"> -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
    <!-- <script src="https://cdn.rawgit.com/JacobLett/bootstrap4-latest/master/bootstrap-4-latest.min.js"></script> -->

    <link rel="shortcut icon" href="<?php echo base_url('img/favicon.ico');?>">
    <link rel="stylesheet" href="<?php echo base_url('vendor/sweetalert2/dist/sweetalert2.css');?>">
    @yield('moreCSS')
    <style>
    .view-group {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: row;
        flex-direction: row;
        padding-left: 0;
        margin-bottom: 0;
    }
    .thumbnail
    {
        margin-bottom: 30px;
        padding: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .item.list-group-item
    {
        float: none;
        width: 100%;
        background-color: #fff;
        margin-bottom: 30px;
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
        padding: 0 1rem;
        border: 0;
    }
    .item.list-group-item .img-event {
        float: left;
        width: 30%;
    }

    .item.list-group-item .list-group-image
    {
        margin-right: 10px;
    }
    .item.list-group-item .thumbnail
    {
        margin-bottom: 0px;
        display: inline-block;
    }
    .item.list-group-item .caption
    {
        float: left;
        width: 70%;
        margin: 0;
    }

    .item.list-group-item:before, .item.list-group-item:after
    {
        display: table;
        content: " ";
    }

    .item.list-group-item:after
    {
        clear: both;
    }
        /* @media only screen and (min-width: 481px) { */
            /* .grid-group-wrapper.row > [class*="col-"] {
                display: flex;
                flex-direction: column;
            }
            .grid-group-wrapper.row {
                display: flex;
                flex-wrap: wrap;
            }
            .grid-group-wrapper.row:after,
            .grid-group-wrapper.row:before {
                display: flex;
            }
            .grid-group-wrapper.row .thumbnail, .grid-group-wrapper.row .caption {
                display: flex;
                flex: 1 0 auto;
                flex-direction: column;
            }
            .thumbnail {

            display: block;
            padding: 4px;
            margin-bottom: 20px;
            line-height: 1.42857143;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            -webkit-transition: border .2s ease-in-out;
            -o-transition: border .2s ease-in-out;
            transition: border .2s ease-in-out;

            }
            .thumbnail > img {
                margin-right: auto;
                margin-left: auto;
                display: block;
                max-width: 100%;
                height: auto;
            }
            .grid-group-wrapper.row .caption-text {
                flex-grow: 1;
            }
            .grid-group-wrapper.row img {
                width: 100%;
                height: 100%;
                margin-right: auto;
                margin-left: auto;
            }
            .grid-group-wrapper.row .tombol {
                display: flex;
                justify-content: space-between;
            }
            .grid-group-wrapper.row .tombol .lead {
                margin-bottom: 0px;
            }
            .grid-group-wrapper.row .tombol {
                margin-bottom: 10px;
            }
            .list-group-wrapper .item {
                float: none;
                width: 100%;
            }
            .list-group-wrapper .item:before, .list-group-wrapper .item:after {
                display: table;
                content: " ";
            }
            .list-group-wrapper .item:after {
                clear: both;
            }
            .list-group-wrapper .item .thumbnail img {
                margin-right: 10px;
                float: left;
            }
            .list-group-wrapper .item .caption, .list-group-wrapper .item .thumbnail {
                display: flex;
            }
            .list-group-wrapper .item .caption {
                flex-direction: column;
            }
            .list-group-wrapper .item .caption-text {
                flex-grow: 1;
            }
            .thumbnail
            {
                margin-bottom: 20px;
                padding: 0px;
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
            } */
            
        /* } */
        
        
        .modal-dialog {
            max-width: 800px;
            margin: 30px auto;
        }
        .modal-body {
            position:relative;
            padding:0px;
        }
        .close {
            position:absolute;
            right:-30px;
            top:0;
            z-index:999;
            font-size:2rem;
            font-weight: normal;
            color:#fff;
            opacity:1;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        @yield('header')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @yield('sidebar')

        <!-- Content Wrapper. Contains page content -->
        @yield('content')
        <!-- /.content-wrapper -->
        @yield('footer')
        <!-- Control Sidebar -->
        <!-- <aside class="control-sidebar control-sidebar-dark">
        </aside> -->
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url('adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url('adminlte/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('adminlte/plugins/fastclick/fastclick.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('adminlte/dist/js/adminlte.min.js'); ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('adminlte/dist/js/demo.js'); ?>"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url('adminlte/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?php echo base_url('adminlte/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?php echo base_url('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?php echo base_url('adminlte/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo base_url('adminlte/plugins/daterangepicker/daterangepicker.js')?>"></script>
    <!-- bootstrap color picker -->
    <script src="<?php echo base_url('adminlte/plugins/colorpicker/bootstrap-colorpicker.min.js')?>"></script>
    <!-- bootstrap time picker -->
    <script src="<?php echo base_url('adminlte/plugins/timepicker/bootstrap-timepicker.min.js')?>"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo base_url('adminlte/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
    <!-- iCheck 1.0.1 -->
    <script src="<?php echo base_url('adminlte/plugins/iCheck/icheck.min.js')?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('adminlte/plugins/fastclick/fastclick.js')?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('adminlte/dist/js/demo.js')?>"></script>
    <!-- Page script -->
    <script src="<?php echo base_url('vendor/sweetalert2/dist/sweetalert2.min.js');?>"></script>
    <script>
        function list_grid(){
            // $('#list').click(function(event){event.preventDefault();$('#products').addClass('list-group-wrapper').removeClass('grid-group-wrapper');});
            // $('#grid').click(function(event){event.preventDefault();$('#products').removeClass('list-group-wrapper').addClass('grid-group-wrapper');});
            $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
            $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
        }
        
        // $(document).ready(function() {
        //     $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
        //     $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
        // })

        $(document).ready(list_grid())
        function time() {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
            timePicker         : true,
            timePickerIncrement: 30,
            format             : 'MM/DD/YYYY h:mm A'
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
            {
                ranges   : {
                'Today'       : [moment(), moment()],
                'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
            }
            )

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
            showInputs: false
            })
        }
        $(time())
    </script>
    <script>
        $(document).ready(function() {

        // Gets the video src from the data-src on each button

        var $videoSrc;  
        $('.video-btn').click(function() {
            $videoSrc = $(this).data( "src" );
        });
        // console.log($videoSrc);

        // when the modal is opened autoplay it  
        $('#myModal').on('shown.bs.modal', function (e) {
            
        // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
        $("#video").attr('src',$videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" ); 
        })
        
        
        // stop playing the youtube video when I close the modal
        $('#myModal').on('hide.bs.modal', function (e) {
            // a poor man's stop video
            $("#video").attr('src',$videoSrc); 
        }) 
            
        // document ready  
        });
    </script>
    @yield('moreJS')
</body>

</html>
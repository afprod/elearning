<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ base_url('dashboard/index') }} " class="brand-link">
        <img src="{{ base_url('img/logodt3.png') }}" alt="Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">
            @if($this->session->user_login['role'] == 'a01') Admin
            @elseif($this->session->user_login['role'] == 's01') Sekolah
            @elseif($this->session->user_login['role'] == 's02') Siswa
            @endif | Dual Track
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ base_url('dashboard/index') }}" class="nav-link @if($this->router->fetch_class() == 'dashboard/index' && $this->router->fetch_method() == 'index') active @endif">
                        <i class="nav-icon fa fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @if(in_array($this->session->user_login['role'], ['s02']))
                <li class="nav-header">Profil</li>
                <li class="nav-item">
                    <a href="{{ base_url('SiswaController') }}" class="nav-link @if($this->router->fetch_class() == 'SiswaController' && ($this->router->fetch_method() == 'index' || $this->router->fetch_method() == 'editProfil')) active @endif">
                        <i class="nav-icon fa fa-search"></i>
                        <p>Lihat Profil</p>
                    </a>
                </li>
                <li class="nav-header">Dokumen</li>
                <li class="nav-item">
                    <a href="{{ base_url('DuController') }}" class="nav-link @if($this->router->fetch_class() == 'DuController' && $this->router->fetch_method() == 'index') active @endif">
                        <i class="nav-icon fa fa-file-pdf-o"></i>
                        <p>
                            Download Modul
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ base_url('DuController/videoIndex') }}" class="nav-link @if($this->router->fetch_class() == 'DuController' && ($this->router->fetch_method() == 'videoIndex' || $this->router->fetch_method() == 'showVideo')) active @endif">
                        <i class="nav-icon fa fa-film"></i>
                        <p>
                            Video Tutorial
                        </p>
                    </a>
                </li>
                @elseif(in_array($this->session->user_login['role'], ['a01']))
                <li class="nav-header">Data Siswa</li>
                <li class="nav-item">
                    <a href="{{ base_url('AdminController/profilSiswa') }}" class="nav-link @if($this->router->fetch_class() == 'AdminController' && $this->router->fetch_method() == 'profilSiswa') active @endif">
                        <i class="nav-icon fa fa-user"></i>
                        <p>Profil Siswa</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ base_url('AdminController/userSiswa') }}" class="nav-link @if($this->router->fetch_class() == 'AdminController' && $this->router->fetch_method() == 'userSiswa') active @endif">
                        <i class="nav-icon fa fa-users"></i>
                        <p>User Siswa</p>
                    </a>
                </li>
                <li class="nav-header">Manajemen Media</li>
                <li class="nav-item">
                    <a href="{{ base_url('AdminController/modulIndex') }}" class="nav-link @if($this->router->fetch_class() == 'AdminController' && ($this->router->fetch_method() == 'modulIndex' || $this->router->fetch_method() == 'editModul' || $this->router->fetch_method() == 'tambahModul')) active @endif">
                        <i class="nav-icon fa fa-file-pdf-o"></i>
                        <p>Modul</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ base_url('AdminController/videoIndex') }}" class="nav-link @if($this->router->fetch_class() == 'AdminController' && ($this->router->fetch_method() == 'videoIndex' || $this->router->fetch_method() == 'editVideo' || $this->router->fetch_method() == 'tambahVideo')) active @endif">
                        <i class="nav-icon fa fa-film"></i>
                        <p>Video Tutorial</p>
                    </a>
                </li>
                @elseif(in_array($this->session->user_login['role'], ['s01']))
                <li class="nav-header">Data Siswa</li>
                <li class="nav-item">
                    <a href="{{ base_url('SekolahController/profilSiswa') }}" class="nav-link @if($this->router->fetch_class() == 'SekolahController' && $this->router->fetch_method() == 'profilSiswa') active @endif">
                        <i class="nav-icon fa fa-user"></i>
                        <p>Profil Siswa</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ base_url('SekolahController/userSiswa') }}" class="nav-link @if($this->router->fetch_class() == 'SekolahController' && $this->router->fetch_method() == 'userSiswa') active @endif">
                        <i class="nav-icon fa fa-users"></i>
                        <p>User Siswa</p>
                    </a>
                </li>
                <li class="nav-header">Manajemen Media</li>
                <li class="nav-item">
                    <a href="{{ base_url('SekolahController/modulIndex') }}" class="nav-link @if($this->router->fetch_class() == 'SekolahController' && ($this->router->fetch_method() == 'modulIndex' || $this->router->fetch_method() == 'editModul' || $this->router->fetch_method() == 'tambahModul')) active @endif">
                        <i class="nav-icon fa fa-file-pdf-o"></i>
                        <p>Modul</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ base_url('SekolahController/videoIndex') }}" class="nav-link @if($this->router->fetch_class() == 'SekolahController' && ($this->router->fetch_method() == 'videoIndex' || $this->router->fetch_method() == 'editVideo' || $this->router->fetch_method() == 'tambahVideo')) active @endif">
                        <i class="nav-icon fa fa-film"></i>
                        <p>Video Tutorial</p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
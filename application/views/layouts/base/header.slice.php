<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="{{ base_url('frontpage/index') }}"><i class="fa fa-home"> Kembali ke Halaman Depan</i></a>
        </li>
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                @if(in_array($this->session->user_login['role'], ['a01']))
                    <i class="fa fa-user"> {{ $this->session->user_login['first_name'] }}</i>
                @elseif(in_array($this->session->user_login['role'], ['s01']))
                    <i class="fa fa-user"> {{ $biodata->nama_sekolah }}</i>
                @else
                    <i class="fa fa-user"> {{ $biodata->nama_siswa }}</i>
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <div class="dropdown-divider"></div>
                <a href="{{ base_url('Password/index') }}" class="dropdown-item">
                    <i class="fa fa-pencil mr-2"></i> Ganti Password
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ base_url('Sessions/logout') }}" class="dropdown-item">
                    <i class="fa fa-caret-square-o-left mr-2"></i> Logout
                </a>
                <div class="dropdown-divider"></div>
            </div>
        </li>
    </ul>
</nav>